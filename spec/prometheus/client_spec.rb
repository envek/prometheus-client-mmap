# encoding: UTF-8

require 'prometheus/client'

describe Prometheus::Client do
  describe '.registry' do
    it 'returns a registry object' do
      expect(described_class.registry).to be_a(described_class::Registry)
    end

    it 'memorizes the returned object' do
      expect(described_class.registry).to eql(described_class.registry)
    end
  end

  describe '.reset!' do
    let(:metric_name) { :room_temperature_celsius }
    let(:label) { { room: 'kitchen' } }
    let(:value) { 21 }

    it 'clears any existing gauges' do
      registry = described_class.registry

      gauge = Prometheus::Client::Gauge.new(metric_name, 'test')
      gauge.set(label, value)
      registry.register(gauge)

      expect(registry.metrics.count).to eq(1)
      expect(registry.get(metric_name).get(label)).to eq(value)

      described_class.reset!

      registry = described_class.registry
      expect(registry.metrics.count).to eq(0)

      registry.register(gauge)
      expect(registry.get(metric_name).get(label)).not_to eq(value)
    end
  end
end
